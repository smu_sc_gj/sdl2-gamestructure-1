#include "Game.h"
#include "TextureUtils.h"

//For printf()
#include <stdio.h>

bool initGame(Game* game)
{
     game->gameWindow = SDL_CreateWindow("Hello CIS4008",   // Window title
                            SDL_WINDOWPOS_UNDEFINED, // X position
                            SDL_WINDOWPOS_UNDEFINED, // Y position
                            game->WINDOW_WIDTH,            // width
                            game->WINDOW_HEIGHT,           // height               
                            SDL_WINDOW_SHOWN);       // Window flags

   
    
    if(game->gameWindow != nullptr)
    {
        // if the window creation succeeded create our renderer
        game->gameRenderer = SDL_CreateRenderer(game->gameWindow, 0, 0);

        if(game->gameRenderer == nullptr)
        {
          printf("Error - SDL could not create renderer\n");
          return false;
        }
    }
    else
    {
        // could not create the window, so don't try and create the renderer. 
        printf("Error - SDL could not create Window\n");
        return false;
    }
    
     // Track Keystates array
	game->keyStates = SDL_GetKeyboardState(NULL);
      
    /**********************************
     *    Setup background image     *
     * ********************************/

    // Create background texture from file, optimised for renderer 
    game->backgroundTexture = createTextureFromFile("assets/images/background.png", game->gameRenderer);


    /**********************************
     * Setup Player
     * ********************************/

    initPlayer(&game->player,game->gameRenderer);


    return true;
}

void cleanUpGame(Game* game)
{
    //Clean up!
    destPlayer(&game->player);  
   
    SDL_DestroyTexture(game->backgroundTexture);
    game->backgroundTexture = nullptr;
    
    SDL_DestroyRenderer(game->gameRenderer);
    game->gameRenderer = nullptr;
    
    SDL_DestroyWindow(game->gameWindow);
    game->gameWindow = nullptr;   
}

void drawGame(Game* game)
{
     // 1. Clear the screen
    SDL_SetRenderDrawColor(game->gameRenderer, 0, 0, 0, 255);
    // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)

    SDL_RenderClear(game->gameRenderer);

    // 2. Draw the scene
    SDL_RenderCopy(game->gameRenderer, game->backgroundTexture, NULL, NULL);

    drawPlayer(&game->player, game->gameRenderer);

    // 3. Present the current frame to the screen
    SDL_RenderPresent(game->gameRenderer);
}

void updateGame(Game* game, float timeDelta)
{
    updatePlayer(&game->player, timeDelta);
}

void processInputs(Game* game)
{
    // Made this local
    // only processed here. 
    SDL_Event event;

    // Handle input 
    if( SDL_PollEvent( &event ))  // test for events
    { 
        switch(event.type) 
        { 
            case SDL_QUIT:
                game->quit = true;
            break;

            // Key pressed event
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    game->quit = true;
                    break;
                }
            break;

            // Key released event
            case SDL_KEYUP:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    //  Nothing to do here.
                    break;
                }
            break;
            
            default:
                // not an error, there's lots we don't handle. 
                break;    
        }
    }

    // Process Inputs - for player (bad function naming!)
    processInput(&game->player, game->keyStates);
}

void runGameLoop(Game* game)
{

    // Timing variables
    unsigned int currentTimeIndex; 
    unsigned int timeDelta;
    float timeDeltaInSeconds;
    unsigned int prevTimeIndex;

    // initialise preTimeIndex
    prevTimeIndex = SDL_GetTicks();

    // Game loop
    while(!game->quit) // while quit is not true
    { 
	    // Calculate time elapsed
        // Better approaches to this exist 
        //- https://gafferongames.com/post/fix_your_timestep/
        currentTimeIndex = SDL_GetTicks();	
        timeDelta = currentTimeIndex - prevTimeIndex; //time in milliseconds
        timeDeltaInSeconds = timeDelta * 0.001f;
        	
        // Store current time index into prevTimeIndex for next frame
        prevTimeIndex = currentTimeIndex;

        // Process inputs
        processInputs(game);

        // Update Game Objects
        updateGame(game, timeDeltaInSeconds);

        //Draw stuff here.
        drawGame(game);
    }
}
